name = "couchdb"
version = "1.0.0"
authors = ["couchdb-python"]
description = "python binding for couchdb"
# tools = [""]
# requires = ["python"]
# help = "file://{root}/help.html"
uuid = "bb83c747-1d7b-4bc1-bc01-d2fef4c6cb5d"
def commands():
	env.PYCOUCHDB.append("$BD_SOFTS/couchdb-python/{version}")
	env.PYTHONPATH.append("$BD_SOFTS/couchdb-python/{version}")
